CURSO: Marketing Digital
=============

Docente: Pablo Ezequiel Vega. <br>
Coordinadores: Dr. Cristian Martínez. Lic. Carlos Ismael Orozco

Acerca del curso: El mismo forma parte de una vinculación entre el Departamento de Informática ([DIUNSa](http://di.unsa.edu.ar)) y la Escuela de Arte y Oficio de la Provincia de Salta.

**Condiciones para la obtención de certificado:**
- Certificado de asistencia: un 70% de asistencia mínimo a las clases
- Constancia de aprobación: un 60% de asistencia mínimo a clases y aprobación de un trabajo integrador.

**Temario**

1. Introducción al marketing digital
2. Facebook for Business
3. Instagram for Business
4. Herramientas de publicación: Canvas
5. WhatsApp Business
6. Mercado Pago
7. Google My Business
8. Plataforma compra en tu barrio 

**Recursos**
<br> [Plantilla informe ](https://docs.google.com/document/d/1QvhFjyOW2Atpn4ZrZ4l-sW09Jx0Dz3GUhQBmjuy7h1M/edit)

# Semana 1: Introducción
Clase 1: Presentación
- [Diapositiva](https://gitlab.com/pablo2443/marketing-digital-octubre/-/blob/master/Clase1.pdf) <br>
- [Video Comision 1](https://youtu.be/V27ZWaYZsVU) <br>
- [Video Comision 2](https://youtu.be/iu6HZks9gNI) <br>

Clase 2: Facebook for Business
- [Video Comision 1](https://youtu.be/XQHssbuWusE) <br>
- [Video Comision 2](https://youtu.be/UskcPhhGx1U) <br>

# Semana 2:
Clase 3: Instagram for Business
- [Video Comision 1](https://youtu.be/gs6K12Q3zVA) <br>
- [Video Comision 2](https://youtu.be/jsOpr1KLqD0) <br>

Clase 4: Herramientas para posteos: https://www.canva.com/
- [Video Comision 1](https://youtu.be/T2xjn8ObfDc) <br>
- [Video Comision 2](https://youtu.be/j0p7Q5LuMBs) <br>

# Semana 3:
Clase 5: WhatsApp Bussiness / Mercado Pago
- [Video Comision 1](https://youtu.be/tjRgjG5As68) <br>
- [Video Comision 2](https://youtu.be/pnI5RkaJPic) <br>

Clase 6: Directorios locales: Google my Business
- [Video Comision 1](https://youtu.be/k9x8jejzyWk) <br>
- [Video Comision 2](https://youtu.be/rNxP5XFRKN4) <br>


# Semana 4:
Clase 7: Compra en tu barrio
- [Video Comision 1](https://youtu.be/wdOFS1QsVC8) <br>
- [Video Comision 2](https://youtu.be/UjecQjlwGFY) <br>

# Plantilla informe:
<br> [Plantilla informe ](https://docs.google.com/document/d/1QvhFjyOW2Atpn4ZrZ4l-sW09Jx0Dz3GUhQBmjuy7h1M/edit)

# Exposiciones:
- [Video](https://youtu.be/hkxJvWUmq4Q) <br>
